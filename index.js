const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
// const MongoClient = require('mongodb').MongoClient;
// const format = require('util').format;
// const url = 'mongodb://localhost:27017/test';
// const mongoose = require('mongoose');
//
// mongoose.connect('mongodb://localhost/users')
//   .then(()=> console.log('start'))
//   .catch((e)=> console.log(e))

let arreyUser = [];
let history = [];
let usersArr = [];
let chatArr = [];
let arrRooms = [];
app.get('/',function(req, res){
  res.sendFile(__dirname + '/public/index.html');
//  res.sendFile(__dirname + '/public/style.css');
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

io.on('connection', function(socket){
  console.log('connected new client');
  console.log(socket.id);

  socket.on('createClient', function(login, password, url){
    let error = false;
    usersArr.map( (e) => {
      if(e.login == login){
          error = true;
      }
    });
    if(error == true){
      console.error('логін вже використовується');
    }
    else{
      console.log('POTRACENO');
        let newUser = {login: login, password: password, url: url};
        usersArr.push(newUser);
        let arrJSON = JSON.stringify(usersArr);
        console.log('userJSON', arrJSON);
        io.emit('saveClient', arrJSON);
    }
  });
  socket.on('connectedUser', function(data){
    console.log(data.name);

    ////baza  dannix
    let person = {content: data, id:socket.id};


    arreyUser.push(person);
    const sablonUser = `
        <img class='avatar' data-name='${data.login}' src= '${data.url}'/>
        <h1 class ='nik'>${data.login}</h1>
    `
    io.emit('newConnectedUser', sablonUser);
  });
  socket.on('poisk', function(data){

    arreyUser.map( function(e){
        if(e.content.login == data){
            generShablon(e);
        }
      });
      chatArr.map( (e)=>{
        if(e.name == data){
          genetShablonChat(name);
        }
      });
    });
   function generShablon (arrName){
      let shablonPoisk = `
        <hr>
        <li class='activUser' data-id=${arrName.id}>
          <img class='avatarHoost' data-name='${arrName.content.login}' src='${arrName.content.url}'>
          ${arrName.content.login}
        </li>
        <hr>
      `;
      io.emit('shablonPoisk', shablonPoisk);
  }

  socket.on('message', function(test, id){
    arreyUser.map((e)=>{
      if(socket.id == e.id){
        let shablonMessage =`
          <ul class='messElem'>
            <hr>
            <li class='border'><img class='avatarHoost' src='${e.content.url}'>${e.content.login}</li>
            <li>${test}</li>
            <hr>
          </ul>
        `
        let newHistory = {text: shablonMessage, id: id };
        history.push(newHistory);
        io.sockets.in(id).emit('newMessage', shablonMessage);
      }
    });

  });
  socket.on('getAvatar', function(url, name){
    usersArr.map((e)=>{
      if(e.login == name){
          e.url = url;
          console.log('test', e);
      }
    });

    let arrJSON = JSON.stringify(usersArr);
    console.log('arrJSON',arrJSON);
    io.emit('saveClient', arrJSON);
  });
  socket.on('onChat', function(id){
    let saveHistory = history.map((e)=>{
      if(e != 'undefined'){
        if(e.id == id){
          console.log('yra');
          return e.text;
        }
      }

    });
    arreyUser.map((e)=>{
      if(e.id == id){
        let shablonHeder = `
              <div id="userHost">
                <img class="avatarHost" src='${e.content.url}' alt="">
                <h1 class ='nik'>${e.content.login}</h1>
              </div>
            `;
        console.log('history',history);
        io.emit('generHosts',shablonHeder, saveHistory)
      }
    });
  });

  socket.on('getRoom', function(name){

    arrRooms.push(name);

    console.log(chatArr);
    let headerRoom = `
        <h1 class='nameGrups'>${name}</h1>
        <input type="text" class='roomsPoisk' placeholder="Пошук">
        <input type='button' class='roomsButton' value='Добавить'>
    `
    let listRooms =`
      <li class='listRooms'>${name}</li>
    `
    io.emit('setRoom', headerRoom, listRooms);
  });
  function genetShablonChat(name){
    let listRooms =`
      <li class='listRooms'>${name}</li>
    `
    io.emit('shablonPoisk', listRooms);
  }
  io.emit('newChat', function(text, nameG){
    arreyUser.map((e)=>{
      if(e.login == text){
        socket.join(nameG);
        let shablonUsers = `
          <li><img src='${e.url}'>${e.login}</li>
        `
        io.emit('updatechat', nameG, shablonUsers);
      }
    });
  });
  socket.on('notConect', function(user){
    console.log('user',socket.id);
  });

});///end

///clouse
